import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular1';

  name = "imron"
  age = 23
  status = false

  datas = [1,2,3]

  personList = [
    {
      title: 'Test A',
      name: 'FEFE',
      age: 22,
      status: true
    },
    {
      title: 'Test B',
      name: 'GILANG',
      age: 26,
      status: false
    }
  ]

  person = {
    title: 'Test A',
    name: 'FEFE',
    age: 22,
    status: true
  }

  person2 = {
    title: 'Test B',
    name: 'GILANG',
    age: 26,
    status: true
  }

  constructor() {
    this.name = "jago"
    this.age = 22
  }

  onCallBack(ev: any) {
    console.log(ev);
    this.personList.push(ev.data)
  }
}
