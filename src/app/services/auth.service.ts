import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { RequestLogin, ResponseLogin } from '../interfaces/auth.interface';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseApi = 'http://localhost:8080'

  public isAdmin = new BehaviorSubject<boolean>(false)
  public isUser = new BehaviorSubject<boolean>(false)

  constructor(private httpClient: HttpClient) { }

  login(payload: RequestLogin): Observable<ResponseLogin> {
    return this.httpClient.post<ResponseLogin>(this.baseApi + '/login', payload).pipe(
      tap( val=> {
        console.log(val)
        if(val.role === 'admin') {
          this.isAdmin.next(true)
        } else if(val.role === 'user') {
          this.isUser.next(true)
        }
      })
    )
  }

  register(payload: User): Observable<User> {
    return this.httpClient.post<User>(this.baseApi + '/register', payload)
  }

}
