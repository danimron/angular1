import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, map, catchError } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface'; 
// import { User } from '../interfaces/user.interface';

import { ResponseUploadPhoto, User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getUserList(): Observable<User[]> {

    // const token = localStorage.getItem('token')

    // const header = new HttpHeaders({
    //   'Authorization': `Bearer ${token}`
    // })

    return this.httpClient
      .get<ResponseBase<User[]>>(`${this.baseApi}/api/users/`, {
        // headers: header,
      })
      .pipe(
        map( val => {
          return val.data
        }),
        catchError((err) => {
          console.log(err)
          throw err
        })
      )
  }

  uploadPhoto(data: File): Observable<ResponseBase<string>>{
    
    const file = new FormData()
    file.append('file', data)

    return this.httpClient.post<ResponseBase<string>>(`${this.baseApi}/api/upload_file`, file)
  }


}
