import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() title = 'angular1';

  @Input() name = "imron"
  @Input() age = 23
  @Input() status = false

  @Output() dataCallBack = new EventEmitter()

  doClick() {
    this.dataCallBack.emit({ data: { name: this.name, age: this.age, status: this.status }})
  }
}
