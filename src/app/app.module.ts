import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { LoginComponent } from './pages/login/login.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
import { RegisterComponent } from './pages/register/register.component';
import { CalculateGasolineComponent } from './pages/calculate-gasoline/calculate-gasoline.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UsersComponent } from './pages/users/users.component';
import { HttpInterceptorCore } from './core/interceptor.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    LoginComponent,
    PlaygroundComponent,
    RegisterComponent,
    CalculateGasolineComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorCore,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
