import { Component } from '@angular/core';
import { User } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';
import { LoginComponent } from '../../login/login.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  user: User[] = []
  constructor(private userService: UserService ) {
    this.userService.getUserList().subscribe(
      response => {
        console.log(response)
        this.user = response
      }
    )
  }
}

