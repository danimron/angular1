import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {
  title = 'angular1';

  name = "imron"
  age = 23
  status = false

  datas = [1,2,3]

  showData = true

  nomor = 1

  personList = [
    {
      title: 'Test A',
      name: 'FEFE',
      age: 22,
      status: true
    },
    {
      title: 'Test B',
      name: 'GILANG',
      age: 26,
      status: false
    }
  ]

  person = {
    title: 'Test A',
    name: 'FEFE',
    age: 22,
    status: true
  }

  person2 = {
    title: 'Test B',
    name: 'GILANG',
    age: 26,
    status: true
  }

  constructor() {
    this.name = "jago"
    this.age = 22
    this.status = false
  }

  onCallBack(ev: any) {
    console.log(ev);
    this.personList.push(ev.data)
  }
}
