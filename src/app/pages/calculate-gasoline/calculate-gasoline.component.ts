import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss']
})
export class CalculateGasolineComponent {
  testClass = 'alert alert-success'

  status = ""
  jenisBensin = 0
  hargaBensin = 0 
  jumlahBensin = 0
  jumlahBayar = 0
  hargaTotal = 0
  kembalian = 0

  constructor(private dataService: DataService){
    this.status = this.dataService.status
  }
  
  hitungHargaBensin(){
    switch (this.jenisBensin) {
      case 1:
        this.hargaBensin = 1000
        console.log(this.hargaBensin)
        break;

      case 2:
        this.hargaBensin = 2000
        break

      case 3:
        this.hargaBensin = 3000
        break
      
      case 4:
        this.hargaBensin = 4000
        break
    
      default:
        break;
    }
  }

  doClick() {
    this.hitungHargaBensin()
    this.hargaTotal = this.jumlahBensin * this.hargaBensin
    this.kembalian = this.jumlahBayar - this.hargaTotal
  }
}


