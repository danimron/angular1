import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  
  formLogin: FormGroup

  constructor(
    private authService: AuthService, 
    private router: Router,
    private formBuilder: FormBuilder){
      this.formLogin = this.formBuilder.group({
        username: ['',[Validators.required, Validators.email]],
        password: ['',[Validators.required, Validators.minLength(6)]]
      })
    }
  
  get errorControl(){
    return this.formLogin.controls
  }


  doLogin(){
    console.log(this.formLogin)

    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password
    }
    
    this.authService.login(payload).subscribe(
      response => {
        console.log(response)
        // if (response.token) {
        //   this.router.navigate(['/playground']);
        // }
        alert("You have successfully log in")
          if(response.role === 'admin'){
            localStorage.setItem('token', response.token)
            this.router.navigate(['/admin/dashboard'])
          } else if(response.role === 'user') {
            this.router.navigate(['/users'])
          } else {
            this.router.navigate(['/playground'])
          }


        // alert(response.role)
        // if(response.role === 'ROLE_ADMIN'){
        //   this.router.navigate(['/admin'])
        // } else if(response.role === 'ROLE_USER') {
        //   this.router.navigate(['/users'])
        // } else {
        //   this.router.navigate(['/playground'])
        // }
      }, error => {
        console.log(error)
        alert(error.error.message)
      }
    )
  }
}
