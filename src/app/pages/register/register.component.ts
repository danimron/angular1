import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { map, switchMap } from 'rxjs';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  formRegister: FormGroup
  photoFile!: File

  constructor(
    private authService: AuthService, 
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder){
      this.formRegister = this.formBuilder.group({
        username: ['',[Validators.required, Validators.email]],
        password: ['',[Validators.required, Validators.minLength(6)]],
        c_password: [''],
        name: [''],
        birthdate: [''],
        gender: [''],
        photo: ['']
      })
    }
  
  get errorControl(){
    return this.formRegister.controls
  }

  get confirmPassword(){
    return this.formRegister.value.password == this.formRegister.value.c_password 
  }

  doRegister(){
    console.log(this.formRegister)

    this.userService
      .uploadPhoto(this.photoFile)
      .pipe(
        switchMap((val) => {
          const payload = {
            username: this.formRegister.value.username,
            password: this.formRegister.value.password,
            role: 'admin',
            name: this.formRegister.value.name,
            birthdate: this.formRegister.value.birthdate,
            gender: this.formRegister.value.gender,
            photo: val.data
          }
          return this.authService.register(payload).pipe(map((val) => val));
        })
      )
      .subscribe((response) => {
        console.log(response);
      });

  }
}
