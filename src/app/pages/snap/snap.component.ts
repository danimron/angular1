import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-snap',
  templateUrl: './snap.component.html',
  styleUrls: ['./snap.component.scss']
})
export class SnapComponent {
  
  formAuth: FormGroup

  constructor(
    private formBuilder: FormBuilder){
      this.formAuth = this.formBuilder.group({
        clientId: ['',[Validators.required]],
        timestamp: ['',[Validators.required]],
        privateKey: ['',[Validators.required]]
      })
    }

  generateSignatureAuth(){
    const clientId = this.formAuth.get('clientId')?.value;
    const timestamp = this.formAuth.get('timestamp')?.value;
    const privateKey = this.formAuth.get('privateKey')?.value;

    const data = `${clientId}&${timestamp}`;

    // const signature = this.generateSignature(data, privateKey);

    // console.log(signature);
  }

}
