export interface RequestLogin {
    username: string,
    password: string
}

export interface ResponseLogin {
    token: string,
    role: string
}