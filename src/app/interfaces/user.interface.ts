export interface User {
    username: string,
    password: string,
    role: string,
    name: string,
    birthdate: Date,
    gender: string,
    photo?: string
  }

  export interface ResponseUploadPhoto{
    image: string
}