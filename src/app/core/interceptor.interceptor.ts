import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpInterceptorCore implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = localStorage.getItem('token')

    //Check for url. If it is login or register url then return    
    if (request.url.includes('/login')) {
      return next.handle(request);
    } else if (request.url.includes('/register')) {
      return next.handle(request);
    }

    if(token) {
      // console.log("sini");
      request = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${token}`)
      })
    }

    return next.handle(request);
  }
}
